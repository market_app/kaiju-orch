"""
This module may contain various information (constants, structures, etc.)
used by multiple other classes. For example: error codes, permission keys,
text templates. It's recommended to keep things simple in here and don't use
complicated logic or many imports.
"""
