import asyncio
import pytest
import uuid
from functools import partial

from kaiju_tools.tests.fixtures import *
from kaiju_cache.tests.fixtures import *
from ..services import RedisLocksService
from ..exceptions import *


async def test_RedisLocksService(redis, application, redis_cache, logger):
    """
    Use it as an example for your own tests.

    :param keydb: keydb docker container fixture
    :param web_application: aiohttp webapp fixture
    :param redis_cace: RedisCache service fixture
    :param logger: python logger instance fixture
    """

    init_script = partial(
        RedisLocksService, app=application(), transport=redis_cache, refresh_interval=0.5)
    await _script(redis, init_script, logger)


async def _script(container, init_script, logger):
    key = 'some_group:some_service'
    value = str(uuid.uuid4())

    logger.info('Initialization.')

    async with init_script(refresh_interval=1, logger=logger) as locks:

        logger.info('Testing basic locking.')

        await locks.acquire(key, value)
        owner = await locks.owner(key)
        assert owner == value
        await locks.release(key, value)

        logger.info('Testing locking/unlocking with ttl')

        await locks.acquire(key, value, ttl=1)
        await locks.acquire(key, value, ttl=1)

        logger.info('Trying to release a lock by a different owner should produce an error.')

        with pytest.raises(NotALockOwnerError):
            new_value = str(uuid.uuid4())
            await locks.release(key, new_value)

        logger.info('Trying to acquire an existing lock with wait=False should raise an error.')

        with pytest.raises(LockExistsError):
            await locks.acquire(key, new_value, wait=False)

        logger.info('Testing wait operation timeout.')

        with pytest.raises(LockAcquireTimeout):
            await locks.wait(key, timeout=RedisLocksService.WAIT_RELEASE_REFRESH_INTERVAL)

        logger.info('Testing wait without acquire.')
        await locks.wait(key, timeout=1)

        logger.info('Trying to set multiple locks at once (should raise an error for all except one)')

        async with init_script(refresh_interval=1, logger=logger) as locks2:
            async with init_script(refresh_interval=1, logger=logger) as locks3:

                results = await asyncio.gather(
                    locks.acquire('key', uuid.uuid4(), wait=False),
                    locks2.acquire('key', uuid.uuid4(), wait=False),
                    locks3.acquire('key', uuid.uuid4(), wait=False),
                    return_exceptions=True)

        counter = 0

        for value in results:
            logger.debug(value)
            if isinstance(value, LockExistsError):
                counter += 1

        assert counter == 2

        logger.info('Testing for possible short connection drop.')

        container._container.pause()
        task = asyncio.create_task(locks.acquire('some_other_key', value))
        await asyncio.sleep(1)
        container._container.unpause()
        await task

        logger.info('Terminating.')
