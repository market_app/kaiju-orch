"""
A service class to manage locks between multiple apps via a shared storage.

How to use
----------

There are two options: to acquire a lock and to release it.

.. code-block:: python

    await locks.acquire('some_lock', 'my_service')
    await locks.release('some_lock', 'my_service')

A service name must match otherwise it will throw a `LockError` because a lock
can be released only by its owner.

The lock service automatically sets lock TTLs to a small value (couple of minutes)
and renews it periodically. It should prevent locks from hanging if an app
quits unexpectedly.

Usage in your project
---------------------

It is expected to be use with a service initialization system, but you can
use it directly if you provide an aiohttp app instance and will call init/close
manually.

Tests
-----

The tests use **Docker** to set up a Redis or other environment, i.e. you need
to install the docker engine to be able to actually test.

Implementation
--------------

It consists of a base class implementing the skeleton of the methods and specific
backend-oriented classes.

.. uml::

    @startuml

    BaseLocksService <|-- RedisLocksService

    @enduml

The class stores all true TTLs locally and creates a keys with short lifetimes
(~1min) which should prevent from locking when app unexpectedly exits. Each
key stores its owner identifier so only the owner can release the key (obviously
you can omit this behaviour by specifying the same owner id for all the locks).

Thus the format is:

    '<namespace>:<lock_prefix>:<lock_key>' = '<owner_id>'

For example:

    'search:locks:index_123' = 'a56edf7c-c87b-4fda-a4c8-5a856f1c31a7'

Package structure
-----------------

- abc - base classes
- etc - other data (status codes, shared constants, ...)
- exceptions - exception classes used specifically by the locks service
- services - used to register services in the service manager, all service classes must be imported there
- tests - tests for lock services
- redis, ... - each backend specific class should use its own clearly named module file

Implementing your own backend
-----------------------------

Inherit from `BaseLocksService` and implement all the abstract async methods
for your backend. They should raise a proper error classes in specific cases
(read the methods description!). You also may want to change `__init__` a bit
for a proper type hinting for your transport. See how `RedisLocksService` does it.

If you did it here in *kaiju-orch* repository then you must add your service class
import into `.services` module to be able to auto-register your service class in
the service manager (for webapp initialization). **Do not** import your class
directly into package `__init__`.

Add your backend into `.tests.test_locks` for testing
(again, see `test_RedisLocksService` for example). You will need to provide
your own container and transport fixtures (or import them from an other kaiju package).
"""

from .abc import BaseLocksService
from .etc import *
from .exceptions import *
from .services import *
