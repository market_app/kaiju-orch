"""
This module SHOULD usually contain only the package meta-information and
imports from the package's modules.

This docstring SHOULD contain the general package description and the userguide,
because this section is shown the first in the package index.
"""

from .services import *

__version__ = '0.1.0'
__python_version__ = '3.6'
__author__ = 'antonnidhoggr@me.com'
__service_package__ = True
